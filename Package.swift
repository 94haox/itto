// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "itto",
    platforms: [.macOS(.v10_15)],
    dependencies: [
        .package(url: "https://github.com/nicklockwood/SwiftFormat", from: "0.49.0")
    ],
    targets: [
        .executableTarget(
            name: "itto",
            dependencies: ["SwiftFormat"]
        ),
        .testTarget(
            name: "ittoTests",
            dependencies: ["itto"]
        ),
    ]
)
