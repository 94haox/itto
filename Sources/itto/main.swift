import Foundation

let version = "0.0.1"

enum ExitResult: Int32 {
    case success = 0
    case failure = 1
    case systemFailure = 70
}

enum Function: String {
    case mrInfoCheck = "mr_check"
    case formatCheck = "format_check"
}

extension FileHandle: TextOutputStream {
    public func write(_ string: String) {
        write(Data(string.utf8))
    }
}

var stderr = FileHandle.standardError

extension String {
    var inDefault: String { "\u{001B}[39m\(self)" }
    var inRed: String { "\u{001B}[31m\(self)\u{001B}[0m" }
    var inGreen: String { "\u{001B}[32m\(self)\u{001B}[0m" }
    var inYellow: String { "\u{001B}[33m\(self)\u{001B}[0m" }
}

func processArguments(_ args: [String]) -> ExitResult {
    guard args.count > 1 else {
        print("error: missing command argument", to: &stderr)
        print("LayoutTool requires a command argument".inRed, to: &stderr)
        return .failure
    }
    switch args[1] {
    case Function.mrInfoCheck.rawValue:
        print("start mr_info_check")
        guard args.count > 2 else {
            return .failure
        }
        return MRInfoCheck.check(title: args[2])
    case Function.formatCheck.rawValue:
        return CodeStyleCheck.check(args: args)
    default:
        return CodeStyleCheck.check(args: args)
    }
}

// Pass in arguments and exit
let result = processArguments(CommandLine.arguments)
exit(result.rawValue)
