import Foundation

enum MRInfoCheck {}

extension MRInfoCheck {
    enum MRType: String {
        case feat
        case fix
        case doc
        case i18n
        case ref
    }

    static func check(title: String) -> ExitResult {
        if title.hasPrefix(MRType.ref.rawValue) ||
            title.hasPrefix(MRType.doc.rawValue) ||
            title.hasPrefix(MRType.i18n.rawValue)
        {
            return .success
        } else if title.hasPrefix(MRType.fix.rawValue) {
            return .success
        } else if title.hasPrefix(MRType.feat.rawValue) {
            return .success
        } else {
            print("mr 格式错误，请检查")
            return .failure
        }
    }
}
