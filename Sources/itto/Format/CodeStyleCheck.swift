import Foundation
import SwiftFormat

enum CodeStyleCheck {}

extension CodeStyleCheck {
    static func check(args: [String]) -> ExitResult {
        let stderrIsTTY = isatty(STDERR_FILENO) != 0

        let printQueue = DispatchQueue(label: "swiftformat.print")

        CLI.print = { message, type in
            printQueue.sync {
                switch type {
                case .info:
                    print(message, to: &stderr)
                case .success:
                    print(stderrIsTTY ? message.inGreen : message, to: &stderr)
                case .error:
                    print(stderrIsTTY ? message.inRed : message, to: &stderr)
                case .warning:
                    print(stderrIsTTY ? message.inYellow : message, to: &stderr)
                case .content:
                    print(message)
                case .raw:
                    print(message, terminator: "")
                }
            }
        }

        return ExitResult(rawValue: ExitResult(rawValue: SwiftFormat.CLI
                .run(in: FileManager.default.currentDirectoryPath).rawValue)!.rawValue) ?? .failure
    }
}
